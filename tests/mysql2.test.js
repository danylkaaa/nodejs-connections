const { mysql2: mysql2Test } = require('../lib');

const CONFIG = {
  host: 'localhost',
  port: '3307',
  user: 'test',
  password: 'password'
};

describe('mysql2', () => {
  it('should connect to MySQL server successfully using callbacks', (done) => {
    const connection = mysql2Test.connect(CONFIG);
    connection.ping((err) => {
      if (err) {
        return done(err);
      }
      connection.end();
      return done();
    });
  });
  it('should connect to MySQL server successfully using promises', async () => {
    const connection = await mysql2Test.connect(CONFIG, true);
    await connection.ping();
    await connection.end();
  });
  it('should create a pool of connections to MySQL server successfully using callbacks', (done) => {
    const pool = mysql2Test.createPool(CONFIG);
    pool.getConnection((err, connection) => {
      if (err) {
        return done(err);
      }
      return connection.ping((err2) => {
        if (err2) {
          return done(err2);
        }
        connection.release();
        pool.end();
        return done();
      });
    });
  });
  it('should create a pool of connections to MySQL successfully using promises', async () => {
    const pool = await mysql2Test.createPool(CONFIG, true);
    const connection = await pool.getConnection();
    await connection.ping();
    await connection.release();
    pool.end();
  });
});
