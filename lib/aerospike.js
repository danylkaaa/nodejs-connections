const debug = require('debug')('@zulus/connections/aerospike');

/**
 * @typedef PolicyConfig
 * @property {number} socketTimeout
 * @property {number} totalTimeout
 * @property {number} maxRetries
 */

/**
 * @typedef AerospikeHostConfig
 * @property {string} addr
 * @property {number} port
 */

/**
 * @typedef AerospikeConnectionConfig
 * @property {AerospikeHostConfig[]} config.hosts - database hosts
 * @property {string} user - username from database
 * @property {password} password - password from database
 * @property {string|number} authMode- authorization mode from database
 * @property {PolicyConfig} readPolicy - readPolicy config
 * @property {PolicyConfig} writePolicy - writePolicy config
 * @property {number} maxConnsPerNode - maximum number of asynchronous connections allowed for each node.
 * @property {object} modlua - configuration values for the mod-lua user path.
 */

/**
 * @see https://www.aerospike.com/apidocs/nodejs/config.js.html
 * @param {AerospikeConnectionConfig} config
 * @param {boolean} useConsoleForLogging - should Aerospike use console.log for output
 * @returns {*|Promise<AerospikeClient>}
 */
exports.connect = (config, useConsoleForLogging) => {
  const Aerospike = require('aerospike');
  if (useConsoleForLogging) {
    Aerospike.setDefaultLogging(config.log);
  }
  debug('connect with config', config);
  if (!config.readPolicy) {
    config.readPolicy = {
      socketTimeout: 2000,
      totalTimeout: 30000,
      maxRetries: 2
    };
  }
  if (!config.writePolicy) {
    config.writePolicy = {
      socketTimeout: 2000,
      totalTimeout: 30000,
      maxRetries: 2
    };
  }
  const options = {
    user: config.user,
    password: config.password,
    authMode: config.authMode,
    hosts: config.hosts,
    log: {
      level: config.logLevel || Aerospike.log.ERROR
    },
    policies: {
      read: new Aerospike.ReadPolicy(config.readPolicy),
      write: new Aerospike.WritePolicy(config.readPolicy),
    },
    modlua: config.modlua,
    maxConnsPerNode: config.maxConnsPerNode
  };
  return Aerospike.connect(options);
};
