const debug = require('debug')('@zulus/connections/mysql');

/**
 * @typedef MySQLConnectionConfig
 * @property {string} host host of MySQL server
 * @property {string|number} port port of MySQL server
 * @property {string} user username from MySQL server
 * @property {string} password password from MySQL server
 * @property {number} connectTimeout timeout of connection
 * @property {string} database default database from MySQL server
 * @property {boolean} debug use mysql module in debug mode
 */

/**
 * creates a new connection to MySQL
 * @see https://github.com/mysqljs/mysql#connection-options
 * @param {MySQLConnectionConfig} config
 * @param {boolean} usePromise create connection with promises instead of callback
 * @returns {Connection}
 */
exports.connect = (config, usePromise) => {
  let mysql;
  if (usePromise) {
    mysql = require('promise-mysql');
  } else {
    mysql = require('mysql');
  }
  debug('connect with config', config);
  return mysql.createConnection({
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    connectTimeout: config.connectTimeout || 10000,
    debug: config.debug,
  });
};

/**
 * @typedef MySQLPoolConnectionConfig
 * @property {string} host host of MySQL server
 * @property {string|number} port port of MySQL server
 * @property {string} user username from MySQL server
 * @property {string} password password from MySQL server
 * @property {number} connectionLimit maximum number of connections to MySQL server in a pool
 * @property {string} database default database from MySQL server
 * @property {boolean} debug use mysql module in debug mode
 */

/**
 * creates a new pool with MySQL connections
 * @see https://github.com/mysqljs/mysql#pooling-connections
 * @param {MySQLPoolConnectionConfig} config
 * @param {boolean} usePromise create pool with promises instead of callback
 * @returns {Pool}
 */
exports.createPool = (config, usePromise) => {
  let mysql;
  if (usePromise) {
    mysql = require('promise-mysql');
  } else {
    mysql = require('mysql');
  }
  debug('create pool with config', config);
  const connectionMysqlPool = mysql.createPool({
    connectionLimit: config.connectionLimit,
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    database: config.database,
    debug: config.debug
  });

  connectionMysqlPool.on('error', (err) => {
    console.error('@smarty/mysql.createPool %O', err);
  });

  return connectionMysqlPool;
};
