# connections

[![pipeline status](https://gitlab.com/ZulusK/nodejs-connections/badges/master/pipeline.svg)](https://gitlab.com/ZulusK/nodejs-connections/commits/master)
[![coverage report](https://gitlab.com/ZulusK/nodejs-connections/badges/master/coverage.svg)](https://gitlab.com/ZulusK/nodejs-connections/commits/master)

Provides simply wrappers around databases modules for simplifying connection for it.

### Note!
You need to install the database modules by yourself, because  mysql, aerospike, knex and etc. modules was not added into package.json. This was done so as not to force you to install unnecessary modules for your project.

So, if you want to use `connection.mysql`, you need to add `mysql` into your project package.json by hand. 


```bash
npm i @zulus/connections
```

### Structure

- [Usage](#usage)
- [API](#api)
- [Tests](#tests)
- [Install](#install)
  - [Install master branch](#install)
  - [Install custom branch](#install-custom)
- [Contributing rules](#contributing)

<a name="usage"></a>
### USAGE
```js
const connections = require('@zulus/connections');
...
const mysqlPromis = connections.mysql.createPool(config,true)
const mysqlCallback = connections.mysql.createPool(config)
```

<a name="api"></a>
### API
### MySQL
 ### `[mysql,mysql2].connect(config, usePromise)`
 Creates a mysql connection. API for mysql and mysql2 are the same.
 * `config` - configuration object for mysql connection
     * `host` host of MySQL server
     * `port` port of MySQL server
     * `user` username from MySQL server
     * `password` password from MySQL server
     * `connectTimeout` timeout of connection
     * `database` default database from MySQL server
     * `debug` use mysql module in debug mode
 * `usePromise` create connection with promises instead of callback
  ### `mysql.createPool(config, usePromise)`
  Creates a pool of mysql connections
  * `config` - configuration object for mysql connection
      * `host` host of MySQL server
      * `port` port of MySQL server
      * `user` username from MySQL server
      * `password` password from MySQL server
      * `connectionLimit` maximum number of connections to MySQL server in a pool
      * `database` default database from MySQL server
      * `debug` use mysql module in debug mode
  * `usePromise` create connection with promises instead of callback
### Aeroispike
 ### `aerospike.connect(config, usePromise)`
 Creates Aerospike connection.
 * `config` - configuration object for aerospike connection
      * `hosts` - database hosts
      * `user` - username from database
      * `password` - username from database
      * `authMode`- authorization mode from database
      * `readPolicy` - readPolicy config
      * `writePolicy` - writePolicy config
      * `maxConnsPerNode` - maximum number of asynchronous connections allowed for each node.
      * `modlua` - configuration values for the mod-lua user path. 
  * `useConsoleForLogging` - put Aerospike error logs into console.log    
### Knex
 ### `knex.connect(config, [ext])`
 Creates a knex connection to SQL database
 * `config` - configuration object for Knex connection
    * `dialect` - The database version (mysql, mysql2, pg)
    * `databaseVersion` - The version of databse should be used (ex. 5.7 for mysql)
    * `host` - The database address
    * `port` - The port for connection
    * `user` - The user's login from database
    * `password` - The user's password from database
    * `database` - The database name to use as default
    * `poolMin` - The minimum number of connection in a pool
    * `poolMax` - The maximum number of connection in a pool
 * `ext` - any extensions & plugins for Knex    
    
<a name="tests"></a>
### Tests
If you with to run tests, do next steps:
1. Install Docker images and run containers with command:
    ```bash
    docker-compose -f docker-compose.test.yml up app
    ```
2. Install databases modules, not listed in package.json:
    ```bash
    ./install_dependencies_for_test.sh
    ```
3. Run tests:
    ```bash
    npm test
    ```
4. Stop docker containers
    ```bash
    docker-compose -f docker-compose.test.yml down
    ```        
<a name="install"></a>


<a name="contributing"></a>

### Contributing

To start contributing do

```bash
git clone git@gitlab.com:ZulusK/nodejs-connections.git
git checkout develop
git checkout -b <your-branch-name>
```

The project is developed in accordance with the [GitFlow][gitflow-docs] methodology.

##### What it means

1. All work you should do in your own **local** branch (naming is important, look below), then make pull request to **develop** branch
2. Your local branch should not have conflicts with repository **develop** branch. To avoid it, before push to repository, do:
   ```bash
   git pull origin develop
   # resolve all conflicts, if they exists
   git add --all
   git commit -m "fix conflicts"
   git push origin <your-branch-name>
   ```
3. We use next naming of branches:

| branch template                      | description                                                 |
| ------------------------------------ | ----------------------------------------------------------- |
| `feat/<short-feature-name>`          | new feature, ex. `feat-add-connections`                          |
| `fix/<short-fix-name>`               | fix of existing feature, ex. `fix-connections`                   |
| `refactor/<short-scope-description>` | refactor, linting, style changes, ex. `style-update-eslint` |
| `test/<short-scope-descriptiopn>`    | tests, ex. `test-db-connections`                            |
| `docs/<short-scope-descriptiopn>`    | documentation, ex. `test-db-connections`                    |

##### Important, before push

1. We use **eslint** with this [rules][eslint-rules] to lint code, before making pull
   request, lint your code:
   ```bash
   npm run lint
   ```
2. Before making pull request, run tests

   ```bash
   npm run test
   ```

[gitflow-docs]: https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/
[eslint-rules]: .eslintrc
[docs-dir]: docs/overview.md
