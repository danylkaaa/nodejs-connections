const { knex } = require('../lib');
const { expect } = require('chai');

const CONFIG = {
  host: 'localhost',
  port: '3307',
  user: 'test',
  password: 'password',
  dialect: 'mysql',
  poolMin: 1,
  poolMax: 2
};

describe('knex', () => {
  it('should connect to MySQL server successfully', async () => {
    const knexInstance = knex.connect(CONFIG);
    const result = await knexInstance.raw('SELECT version()');
    expect(result).to.be.an('array').and.to.have.length(2);
    knexInstance.destroy();
  });
  it('should connect to MySQL server successfully, without pool options', async () => {
    const knexInstance = knex.connect({
      ...CONFIG,
      poolMax: undefined,
      poolMin: undefined
    });
    const result = await knexInstance.raw('SELECT version()');
    expect(result).to.be.an('array').and.to.have.length(2);
    knexInstance.destroy();
  });
});
