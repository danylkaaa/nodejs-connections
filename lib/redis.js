const debug = require('debug')('@zulus/connections/redis');

/**
 * @typedef {Object} RedisConnectConfig
 * @property {string} host - The database address
 * @property {number} port - The port for connection
 * @property {string} auth - The password from database
 */
/**
 * @see https://www.npmjs.com/package/redis#rediscreateclient
 * @param {RedisConnectConfig} config
 * @returns {RedisClient}
 */
exports.connect = (config) => {
  const redis = require('redis');
  debug('connect to redis with config %O', config);
  return redis.createClient(config);
};
