const { aerospike } = require('../lib');
const { expect } = require('chai');
const Aerospike = require('aerospike');

const CONFIG = {
  hosts: [
    {
      addr: '127.0.0.1',
      port: 3001
    }
  ],
};

describe('aerospike', () => {
  it('should connect to Aerospike server successfully', async () => {
    const connection = await aerospike.connect(CONFIG);
    expect(connection).to.be.an('object').and.instanceOf(Aerospike.Client);
    await connection.close(false);
  });
  it('should should throw error on connect to Aerospike (invalid port)', async () => {
    try {
      await aerospike.connect({
        hosts: [
          {
            addr: '127.0.0.1',
            port: 3003
          }
        ],
      });
      throw new Error('Error was not thrown');
    } catch (e) {
      expect(e).to.have.property('message', 'Failed to connect');
    }
  });
  it('should connect, use readPolicy', async () => {
    const connection = await aerospike.connect({
      ...CONFIG,
      readPolicy: {
        totalTimeout: 1000
      }
    });
    expect(connection).to.be.an('object');
    await connection.close(false);
  });
  it('should connect, use writePolicy', async () => {
    const connection = await aerospike.connect({
      ...CONFIG,
      readPolicy: {
        totalTimeout: 1000
      }
    });
    expect(connection).to.be.an('object').and.instanceOf(Aerospike.Client);
    await connection.close(true);
  });
});
