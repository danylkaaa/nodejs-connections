const debug = require('debug')('@zulus/connections/knex');

/**
 * @typedef {Object} KnexConnectConfig
 * @property {string} dialect - The database version (mysql, mysql2, pg)
 * @property {string} databaseVersion - The version of databse should be used (ex. 5.7 for mysql)
 * @property {string} host - The database address
 * @property {string|number} port - The port for connection
 * @property {string} user - The user's login from database
 * @property {string} password - The user's password from database
 * @property {string} database - The database name to use as default
 * @property {number} poolMin - The minimum number of connection in a pool
 * @property {number} poolMax - The maximum number of connection in a pool
 */

/**
 * @see https://knexjs.org/#Installation-client
 * @param {KnexConnectConfig} config - connection database options
 * @param {any} ext - any knex plugins
 * @returns {Knex.QueryBuilder}
 */
exports.connect = (config, ext) => {
  const knex = require('knex');
  debug('connect with config', config);
  return knex({
    client: config.dialect,
    version: config.databaseVersion,
    connection: {
      host: config.host,
      port: config.port,
      user: config.user,
      password: config.password,
      database: config.database
    },
    pool: { min: config.poolMin || 1, max: config.poolMax || 10 },
    ...ext
  });
};
